# crabjail jailconfs

Jailconfs for crabjail.

## Installation

### Prerequisites

- [meson](https://mesonbuild.com/)

### Install

    meson _builddir && meson install -C _builddir

## Changelog

See [CHANGELOG.md](CHANGELOG.md)

## Authors and Contributors

See [AUTHORS](AUTHORS)

## License

GPL-3.0-or-later, see [COPYING](COPYING) for details.
